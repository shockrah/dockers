#!/bin/bash

# In case you need to setup docker on a Debain system
# NOTE: you will need root access to install most of this stuff
# NOTE: commands taken straight from Docker's official instructions
# LINK: https://docs.docker.com/engine/install/debian/#install-using-the-repository

apt-get install \
	apt-transport-https \
	ca-certificates \
	curl \
	gnupg-agent \
	software-properties-common

mkdir -p binaries
curl 'https://download.docker.com/linux/debian/dists/buster/pool/stable/amd64/containerd.io_1.2.6-3_amd64.deb' -o binaries/containered.io.deb
curl 'https://download.docker.com/linux/debian/dists/buster/pool/stable/amd64/docker-ce_19.03.9~3-0~debian-buster_amd64.deb' -o binaries/docker-ce.deb
curl 'https://download.docker.com/linux/debian/dists/buster/pool/stable/amd64/docker-ce-cli_19.03.9~3-0~debian-buster_amd64.deb' -o binaries/docker-ce-cli.deb

sudo dpkg -i ./binaries/containered.io.deb
sudo dpkg -i ./binaries/docker-ce.deb
sudo dpkg -i ./binaries/docker-ce-cli.deb

