#!/bin/bash

set -e

# This script is a generic builder script which only builds
# images if the latest commit modifies the given folder in some way

target="$1"
tag="$2"

# Ensure we have a target
if [ -z "$target" ]; then
	echo "$target is not defined ( empty )"
	exit 1
fi

# Make sure the target is real
if [ ! -d "$target" ]; then
	echo "$target is not a directory"
	exit 1
fi

# Set the tag if not already set
if [ -z "$tag" ]; then
	tag=latest
	echo "Tag for \"$target\" defaulting to \"latest\""
fi

# Finally set the project root
PROJECT_ROOT=`pwd`

cd "$target"
	echo docker build . -t "$tag"
	echo "${DOCKERHUB_PASSWORD} | docker login --username "${DOCKERHUB_USERNAME}" --password-stdin
cd "$PROJECT_ROOT"
