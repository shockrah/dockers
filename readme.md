# Shockrah's Dockerfiles

This repository serves as a place of consolidation for most of the major public
docker images that I maintain. 

# Building

Workflow for is defined as such in 3 stages:

* Validation stage: where we lint and check any 


# Update Cycle

I mostly update these for my own sake however special versioning cycles can be
arranged for those that that request it.

Email: `mail@shockrah.xyz`


# Contributing

There are really two rules for contributing:

* Be explicit in you descriptions of what is being contributed

State what the contribution is and why it's being proposed. Some contributions
may not be appropriate for a mainline release of an image however it may be
suitable for an alternate release which can always be arranged.

* Don't be a complete ass

_Please don't make me explain this one_.

